import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class softwaresalestest {
 Softwaresales s;
 @Before
 public void setUp(){
	 s=new Softwaresales();
	 
 }
	//R1 Buy one package
	@Test
	public void buyOnetest() {
	
		double finalPrice =s.calculatePrice(1);
		assertEquals(99,finalPrice,0);
	}
	//R2 Buy 10-19 package
	@Test
	public void buy10Test(){
		
		double finalPrice =s.calculatePrice(12);
		assertEquals(950.4,finalPrice,0);
		
	}
	//R3 Buy 20-49
	@Test
	public void buy20Test(){
		
		double finalPrice =s.calculatePrice(30);
		assertEquals(2079,finalPrice,0);
		
	}
	//R4 Buy 50-99
	@Test
	public void buy50Test(){
		
		double finalPrice =s.calculatePrice(60);
		assertEquals(3564,finalPrice,0);
		
		
	}
	//R5 Buy 100+
	@Test
	public void buy100Test(){
		
		double finalPrice =s.calculatePrice(120);
		assertEquals(5940,finalPrice,0);
		
		
	}
	//R6 Buy <0
		@Test
		public void buy0Test(){
			
			double finalPrice =s.calculatePrice(-6);
			assertEquals(-1,finalPrice,0);
			
			
		}

}
